# Robot Framework for Shift Planner

An example project.

## Quickstart

1. Install specified [Python version](.python-version). I recommend using [pyenv](https://github.com/pyenv/pyenv) for managing Python versions (also for [Windows](https://github.com/pyenv-win/pyenv-win)).
2. Create virtual python environment for this project.

    ```console
    python -m venv ./env
    ```

3. Enter the newly created environment

    Powershell

    ```powershell
    .\env\Scripts\Activate.ps1
    ```

    Bash

    ```bash
    source ./env/bin/activate
    ```

4. Install dependencies

    ```console
    pip install -r requirements.txt
    ```

5. Run the test suites

    ```console
    robot .\src\suite\
    ```
