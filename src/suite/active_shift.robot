*** Settings ***
Documentation   Showcase tests for the Active Shifts API

Library         JSONLibrary
Library         OperatingSystem

Resource        ../resource/gherkin_requests.resource
Resource        ../resource/json_helpers.resource

*** Test Cases ***
Active Shifts For Each Team
    Given The Endpoint Is "https://localhost:7214/api/ActiveShift"
    And The Action Is "TechnicianContactInformation"
    When The Query Parameter "id" Is "8"
    And The Request Is Sent
    Then The Response Status Should Be "OK"
    Then The Response Body Is JSON
    And The Response Body Should Have "data.id" Equal To "8"
    And The JSON Should Be Of Schema "TechnicianContactInformation.json"
