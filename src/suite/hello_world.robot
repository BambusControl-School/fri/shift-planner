*** Settings ***
Documentation   Showcase tests for the Hello World API

Resource        ../resource/gherkin_requests.resource

*** Test Cases ***
Hello World
    # ${response}    GET    https://shiftplannerapi.azurewebsites.net/api/HelloWorld/GetHelloWorld
    # Should Be Equal    ${response.text}    Hello World!
    Given The Endpoint Is "https://shiftplannerapi.azurewebsites.net/api/HelloWorld"
    And The Action Is "GetHelloWorld"
    When The Request Is Sent
    Then The Response Message Should Be "Hello World!"

Hello World Object
    Given The Endpoint Is "https://shiftplannerapi.azurewebsites.net/api/HelloWorld"
    And The Action Is "GetHelloWorldObject"
    When The Request Is Sent
    Then The Response Status Should Be "OK"
    And The Response Body Is JSON
    And The Response Body Should Have "fiveTimesThree" Equal To "15"

Throwing A Bone
    Given The Endpoint Is "https://shiftplannerapi.azurewebsites.net/api/HelloWorld"
    And The Action Is "ThrowMeABone"
    When The Request Is Sent
    Then The Response Status Should Be "500"
